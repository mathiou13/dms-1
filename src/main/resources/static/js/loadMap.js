function loadMap() {

    var latitude = $('#latitude').val();
    var longitude = $('#longitude').val();

    var latlng = new google.maps.LatLng(latitude, longitude);
    var myOptions = {
        zoom: 14,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map"),myOptions);

    var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        title:"Incident's spot!"
    });

}