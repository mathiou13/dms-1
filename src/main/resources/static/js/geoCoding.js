$('#search_by_address').click(function() {

    var address = $('#inputAddress').val();
    var zipCode = $('#inputZip').val();

    if(address === ""){
        alert("You must fill address!");
        return false;
    }

    console.log(address+' '+zipCode);

    var key = '21810c1a616385050b732aebe7b19c00';
    var url = new URL("http://api.positionstack.com/v1/forward");

    url.searchParams.append('access_key', key);
    url.searchParams.append('query', address+' '+zipCode);
    url.searchParams.append('country', 'US');
    url.searchParams.append('region', 'Chicago');
    url.searchParams.append('output', 'json');

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": url,
        "method": "GET"
    }

    console.log(url);
    $.ajax(settings).done(function (response) {
        console.log(response);

        var data = null;

        for (var i = 0; i < response['data'].length; i++) {
            if ((response['data'][i]['locality'] != null && response['data'][i]['locality'].includes('Chicago')) ||
                (response['data'][i]['administrative_area'] != null && response['data'][i]['administrative_area'].includes('Chicago'))) {
                data = response['data'][i];
                break;
            }
        }
        if (data == null) {
            data = response['data'][0];
        }

        if (data != null) {
            var latitude = data['latitude'];
            var longitude = data['longitude'];
            var postal_code = data['postal_code'];

            if (zipCode === "") {
                $('#inputZip').val(postal_code);
            }

            $('#longitude').val(longitude);
            $('#latitude').val(latitude);
        }

        loadMap();
    });
});

$('#search_by_coordinates').click(function() {

    var latitude = $('#latitude').val();
    var longitude = $('#longitude').val();

    if(latitude === "" || longitude === ""){
        alert("Latitude and Longitude must be filled!");
        return false;
    }

    var key = '21810c1a616385050b732aebe7b19c00';
    var url = new URL("http://api.positionstack.com/v1/reverse");

    url.searchParams.append('access_key', key);
    url.searchParams.append('country', 'US');
    url.searchParams.append('query', latitude+', '+longitude);
    url.searchParams.append('output', 'json');

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": url,
        "method": "GET"
    }

    $.ajax(settings).done(function (response) {
        console.log(response);

        var data = response['data'][0];
        $('#inputAddress').val(data['label']);
        if (data['postal_code'] == null) {
            for (var i = 0; i < response['data'].length; i++) {
                if (response['data'][i]['postal_code'] != null) {
                    $('#inputZip').val(response['data'][i]['postal_code']);
                    break;
                }
            }
        }

        loadMap();
    });
});