package gr.uoa.dvis.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.sql.Timestamp;
import java.util.List;

@Controller
public class AppController {

    public AppController() {
    }

    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String getIndexPage(Model model) {
        return "index";
    }
}
